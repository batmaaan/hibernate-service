import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
  private static SessionFactory sessionFactory;

  static {

    Configuration configuration = new Configuration()
      .setProperty("hibernate.connection.driver_class",  "org.postgresql.Driver")
      .setProperty("hibernate.connection.url", "jdbc:postgresql://localhost:5432/postgres")
      .setProperty("hibernate.connection.username", "postgres")
      .setProperty("hibernate.connection.password", "1234")
      .setProperty("hibernate.dialect",  "org.hibernate.dialect.PostgreSQLDialect")
      .setProperty("hibernate.show_sql", "true")
      .setProperty("hibernate.hbm2ddl.auto", "create")
      .setProperty("connection.pool_size", "20000")
      .addClass(User.class)
      .addClass(Role.class);


    StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
      .applySettings(configuration.getProperties());

    sessionFactory = configuration.buildSessionFactory(builder.build());
  }

  public static SessionFactory getSessionFactory() {
    return sessionFactory;
  }
}
